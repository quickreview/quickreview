from django.contrib import admin
from django.contrib.auth.models import Group
from .account import User, UserAdmin

admin.site.register(User, UserAdmin)
admin.site.unregister(Group)