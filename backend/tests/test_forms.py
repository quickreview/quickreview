from django.test import SimpleTestCase
from backend.forms import UserAdminChangeForm, UserAdminCreationForm, LoginForm

class TestForms(SimpleTestCase):
    def test_login_form_valid_data(self):
        '''Test whether the login form accepts the correct data types'''
        form = LoginForm(data={
            'email': 'user@email.com',
            'password': 'A1b2C3d4'
        })
        self.assertTrue(form.is_valid())

    def test_login_enforced__email_restrictions(self):
        '''Test whether the login form only allows valid email string'''
        form = LoginForm(data={
            'email': 'user',
            'password': 'A1b2C3d4'
        })
        self.assertFalse(form.is_valid())

    def test_login_enforced_password_restrictions(self):
        '''Test whether the login form enforced password restrictions which only allows password with at least 8 characters and has both letters and numbers
        '''
        short_password = LoginForm(data={
            'email': 'user@email.com',
            'password': '1234',
        })
        self.assertFalse(short_password.is_valid())

        numbers_only = LoginForm(data={
            'email': 'user@email.com',
            'password': '12341234',
        })
        self.assertFalse(numbers_only.is_valid())

        letters_only = LoginForm(data={
            'email': 'user@email.com',
            'password': 'abcdabcd',
        })
        self.assertFalse(letters_only.is_valid())