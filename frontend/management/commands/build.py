from django.core.management.base import BaseCommand
import subprocess
import os

class Command(BaseCommand):
  help = 'Build the front end static files. It will build both the Javscript and CSS files if no argument is passed'

  def add_arguments(self, parser):
    parser.add_argument('-t', '--type', type=str, help='only build a specified type of files, either JS (Javascript) or CSS')
    parser.add_argument('-w', '--watch', action='store_true', help='watch for changes in files')
    parser.add_argument('-c', '--check', action='store_true', help='check if the generated files exists')

  def handle(self,*args, **kwargs):
    filetype = kwargs['type']
    watch = kwargs['watch']
    check = kwargs['check']
    if filetype == None:
      print('Build both JS and CSS')
      if (watch):
        subprocess.run(["npm", "run", "--prefix", "frontend/", "build"])
      else:
        subprocess.run(["npm", "run", "--prefix", "frontend/", "build-js"])
        subprocess.run(["npm", "run", "--prefix", "frontend/", "build-css"])
      if (check):
        checkJSExist()
        checkCSSExist()

    elif filetype.lower() in ('js', 'javascript', 'react', 'reactjs', 'react.js'):
      print('Build JS only')
      if (watch):
        subprocess.run(["npm", "run", "--prefix", "frontend/", "build-js-watch"])
      else:
        subprocess.run(["npm", "run", "--prefix", "frontend/", "build-js"])
      if (check):
        checkJSExist()

    elif filetype.lower() in ('css', 'sass', 'scss'):
      print('Build CSS only')
      if (watch):
        subprocess.run(["npm", "run", "--prefix", "frontend/", "build-css-watch"])
      else:
        subprocess.run(["npm", "run", "--prefix", "frontend/", "build-css"])
      if (check):
        checkCSSExist()

    else:
      print('File type unrecognized')


def checkJSExist():
  pages = []
  for file in os.listdir("./frontend/src/react/"):
    if(file[-3:] == '.js'):
      pages.append(file[:-3])
  for page in pages:
    if(not os.path.isfile(f'./frontend/static/js/{page}.bundle.js')):
      raise Exception(f'{page}.bundle.js does not exists. Check your webpack configuration or the frontend/src/react/ folder')

def checkCSSExist():
  pages = []
  for file in os.listdir("./frontend/src/sass/"):
    if(file[-5:] == '.scss'):
      pages.append(file[:-5])
  for page in pages:
    if(not os.path.isfile(f'./frontend/static/css/{page}.css')):
      raise Exception(f'{page}.css does not exists. Check your package.json or the frontend/src/sass/ folder')
